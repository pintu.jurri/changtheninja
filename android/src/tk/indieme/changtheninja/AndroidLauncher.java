package tk.indieme.changtheninja;

import android.content.Intent;
import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import de.golfgl.gdxgamesvcs.GpgsClient;
import tk.indieme.changtheninja.ChangTheNinjaGame;

public class AndroidLauncher extends AndroidApplication {
    //GPGS game instance
    private GpgsClient gsClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        ChangTheNinjaGame game = new ChangTheNinjaGame(new AndroidStringFormatter());
        //Initlialize gpgs
        gsClient = new GpgsClient().initialize(this, false);
        game.gameServiceClient = gsClient;
        initialize(game, config);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (gsClient != null)
            gsClient.onGpgsActivityResult(requestCode, resultCode, data);
    }
}
