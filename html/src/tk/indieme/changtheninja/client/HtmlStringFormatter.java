package tk.indieme.changtheninja.client;

import com.google.gwt.i18n.client.NumberFormat;
import tk.indieme.changtheninja.StringFormatter;

public class HtmlStringFormatter implements StringFormatter {
    @Override
    public String getFormattedString(int value) {
        NumberFormat format = NumberFormat.getFormat("#.#");
        return format.format(value);
    }
}
