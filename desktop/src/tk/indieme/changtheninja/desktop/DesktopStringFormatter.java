package tk.indieme.changtheninja.desktop;

import tk.indieme.changtheninja.StringFormatter;

import java.util.Locale;

public class DesktopStringFormatter implements StringFormatter {

    @Override
    public String getFormattedString(int value) {

        return String.format(Locale.ENGLISH,"%d", value);
    }
}
