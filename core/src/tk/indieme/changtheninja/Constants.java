package tk.indieme.changtheninja;

public interface Constants {
    int WORLD_WIDTH = 640;
    int WORLD_HEIGHT = 360;
    float SPAWN_POSX = WORLD_WIDTH / 4;
    float SPAWN_POSY = 48;
    float ENEMY_LANDSPAWNY = 16;
    float ENEMY_SPAWNLEFTX = -32;
    float ENEMY_SPAWNRIGHTX = WORLD_WIDTH;
    float ENEMY_SPAWN_TIME = 2;
    int DIRECTION_LEFT=0;
    int DIRECTION_RIGHT =1;

    float GAMEOVER_POSITION = 248;
    float GROUP_POSITION = 48;
    //GPGS
    String LEADERBOARD_ID ="CgkI-bbsg6cEEAIQBg";
}
