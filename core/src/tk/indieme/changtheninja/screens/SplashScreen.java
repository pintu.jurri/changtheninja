package tk.indieme.changtheninja.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import tk.indieme.changtheninja.ChangTheNinjaGame;
import tk.indieme.changtheninja.Constants;

public class SplashScreen extends AbstractScreen {
    private Texture splashTexture;
    private Array<TextureRegion> splashFrames;
    private Animation<TextureRegion> splashAnim;
    private static int FRAME_COUNT=3;
    private float stateTimer;

    private OrthographicCamera camera;

    public SplashScreen(ChangTheNinjaGame game) {
        super(game);
        camera = new OrthographicCamera();
        camera.setToOrtho(false, Constants.WORLD_WIDTH, Constants.WORLD_HEIGHT);

    }

    @Override
    public void show() {
        super.show();
        //Creating UI
        splashTexture = new Texture(Gdx.files.internal("splash.png"));
        TextureRegion[][] frames = TextureRegion.split(splashTexture, 64, 64);
        splashFrames = new Array<TextureRegion>();
        for (int i = 0; i < FRAME_COUNT; i++) {
            splashFrames.add(frames[0][i]);

        }
        splashAnim = new Animation<TextureRegion>(0.10f, splashFrames, Animation.PlayMode.NORMAL);
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        camera.update();
        stateTimer += delta;

        //Check Assetmanager if all assets loaded
        if (game.assetLoader.getAssetManager().update() && splashAnim.isAnimationFinished(stateTimer)) {
            game.assetLoader.load();
            game.setScreen(new StartScreen(game));
            dispose();
        }
        game.batch.setProjectionMatrix(camera.combined);
        game.batch.begin();
        game.batch.draw(splashAnim.getKeyFrame(stateTimer), Constants.WORLD_WIDTH / 2-64, Constants.WORLD_HEIGHT / 2);
        game.batch.end();


    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void pause() {
        super.pause();
    }

    @Override
    public void resume() {
        super.resume();
    }

    @Override
    public void hide() {
        super.hide();
    }

    @Override
    public void dispose() {
        super.dispose();
        splashTexture.dispose();


    }
}
