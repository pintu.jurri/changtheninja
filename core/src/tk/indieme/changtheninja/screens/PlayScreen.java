package tk.indieme.changtheninja.screens;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.viewport.Viewport;
import javafx.print.PageLayout;
import tk.indieme.changtheninja.ChangTheNinjaGame;
import tk.indieme.changtheninja.Constants;
import tk.indieme.changtheninja.EnemyType;
import tk.indieme.changtheninja.Hud;
import tk.indieme.changtheninja.helpers.EnemyFactory;
import tk.indieme.changtheninja.helpers.RandomEnum;
import tk.indieme.changtheninja.models.*;

import java.security.Key;

public class PlayScreen extends AbstractScreen {
    //Camera,Viewport
    private OrthographicCamera camera;
    private Viewport viewport;
    private int shootDirection = 0;

    //Game States
    private enum GameState {
        READY, RUNNING, PAUSED, GAMEOVER
    }

    private GameState currentGameState;
    //World Objects

    private Player player;
    private Ground ground;
    private Hud hud;
    private Array<Enemy> enemies;       //Array Containing enemies
    private RandomEnum<EnemyType> randomEnumGenerator;
    private Vector3 touchPoint;
    private ParticleEffect boomEffect;
    private ParticleEffectPool boomParticleEffectPool, bloodParticleEffectPool, bloodSplatParticleEffectPool;
    private Array<ParticleEffectPool.PooledEffect> pooledEffects;
    private ShapeRenderer shapeRenderer;
    private Music music;
    //Scores
    private float accuracy;
    private int currentScore;
    //Controls
    private boolean isOnDesktop;
    //Spawners,Timers
    private float waveGeneratorTimer, ENEMYSPAWNTIME, enemySpawner;
    //Checkers
    private boolean gameOverShown = false;

    public PlayScreen(ChangTheNinjaGame changTheNinjaGame) {
        super(changTheNinjaGame);
        //Initializing Game World
        camera = new OrthographicCamera(Constants.WORLD_WIDTH, Constants.WORLD_HEIGHT);
        camera.position.set(Constants.WORLD_WIDTH / 2, Constants.WORLD_HEIGHT / 2, 0);
        camera.update();

        currentGameState = GameState.READY;
        //Check Controls
        if (game.gamePref.getString("controls").equalsIgnoreCase("desktop")) {
            isOnDesktop = true;
        } else {
            isOnDesktop = false;
        }
    }

    //Getters
    public Player getPlayer() {
        return player;
    }

    public OrthographicCamera getCamera() {
        return camera;
    }

    //Enemy Pool Containing enemeis
    private final Pool<Enemy> enemyPool = new Pool<Enemy>() {
        @Override
        protected Enemy newObject() {
            //Random Direction
            int direction = MathUtils.random(Constants.DIRECTION_LEFT, Constants.DIRECTION_RIGHT);
            //Returns random enemy
            Enemy enemy = EnemyFactory.getEnemy(Constants.WORLD_WIDTH * direction, 16, direction, game, randomEnumGenerator.random());
            return enemy;
        }
    };

    @Override
    public void show() {
        music = game.assetLoader.mainMusic;
        music.setLooping(true);
        music.play();

        //Initializing game objects
        player = new Player(Constants.WORLD_WIDTH / 2 + 8, Constants.WORLD_HEIGHT / 2, game);
        ground = new Ground(Constants.WORLD_WIDTH / 2, 16, game);
        touchPoint = new Vector3();
        hud = new Hud(game);
        hud.createHUD();

        enemies = new Array<Enemy>();
        randomEnumGenerator = new RandomEnum<EnemyType>(EnemyType.class);

        //Manages Particle Effects
        pooledEffects = new Array<ParticleEffectPool.PooledEffect>();

        //Set up a Particle Effect
        boomEffect = game.assetLoader.blastEffect;
        //TODO : check later
        //boomEffect.setEmittersCleanUpBlendFunction(false);
        boomParticleEffectPool = new ParticleEffectPool(boomEffect, 1, 6);
        bloodParticleEffectPool = new ParticleEffectPool(game.assetLoader.bloodEffect, 1, 6);
        bloodSplatParticleEffectPool = new ParticleEffectPool(game.assetLoader.bloodSplatEffect, 1, 6);
        shapeRenderer = new ShapeRenderer();


    }

    @Override
    public void render(float delta) {
        updateScene(delta);
        drawScene();
        if (game.debug) {
            drawDebug();
        }
        hud.stage.act();
        hud.stage.draw();

    }

    private void drawScene() {
        game.batch.setProjectionMatrix(camera.combined);
        game.batch.begin();
        game.batch.disableBlending();
        game.batch.draw(game.assetLoader.background01, 0, 0);
        game.batch.enableBlending();
        player.draw(game.batch);
        for (Enemy enemy : enemies) {
            enemy.draw(game.batch);

        }
        ground.draw(game.batch);
        //Updating and Drawing Particle Effects
        for (ParticleEffectPool.PooledEffect pooledEffect : pooledEffects) {
            pooledEffect.draw(game.batch, Gdx.graphics.getDeltaTime());
            if (pooledEffect.isComplete()) {
                pooledEffect.free();
                pooledEffects.removeValue(pooledEffect, true);

            }
        }

        //Only draw if Game is in Ready state and on desktop
        if (currentGameState == GameState.READY && isOnDesktop) {
            hud.showDesktopControls();

        } else if (currentGameState == GameState.READY && !isOnDesktop) {
            //else if on mobile
            hud.showMobileControls();

        }
        game.batch.end();
    }

    /*
     * Draws HitBoxes;
     */
    private void drawDebug() {
        shapeRenderer.setProjectionMatrix(camera.combined);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        player.drawDebug(shapeRenderer);
        for (Enemy enemy : enemies) {
            enemy.drawDebug(shapeRenderer);
        }
        shapeRenderer.end();
    }

    private void updateScene(float delta) {
        switch (currentGameState) {
            case READY:
                updateReady();
                break;
            case RUNNING:
                updateRunning(delta);
                break;
            case PAUSED:
            case GAMEOVER:
                updateGameOver(delta);
                break;
        }
    }

    private void updateReady() {
        if (Gdx.input.justTouched()) {
            hud.removeControls();
            currentGameState = GameState.RUNNING;
        }
    }

    private void updateRunning(float delta) {
        waveGeneratorTimer += delta;
        enemySpawner += delta;
        player.shoot = false;


        //Input Handling
        //WEBGL
        Application.ApplicationType applicationType = Gdx.app.getType();
        if (isOnDesktop) {
            //JUMP
            if (Gdx.input.isKeyJustPressed(Input.Keys.UP) && player.isOnGround) {
                player.jump();
                player.isOnGround = false;
            } else if (Gdx.input.isKeyJustPressed(Input.Keys.LEFT)) {
                shootDirection = -1;
                player.shootShuriken(shootDirection);
            } else if (Gdx.input.isKeyJustPressed(Input.Keys.RIGHT)) {
                shootDirection = 1;
                player.shootShuriken(shootDirection);
            }

        } else {
            if (Gdx.input.isTouched()) {

                touchPoint.set(Gdx.input.getX(), Gdx.input.getY(), 0);
                camera.unproject(touchPoint);
                if (touchPoint.x < Constants.WORLD_WIDTH / 2 && player.isOnGround) {
                    player.jump();
                    player.isOnGround = false;
                } else if (touchPoint.x > Constants.WORLD_WIDTH / 2) {
                    if (Gdx.input.getDeltaX() > 0) {
                        shootDirection = 1;
                        player.shootShuriken(shootDirection);
                    } else if (Gdx.input.getDeltaX() < 0) {
                        shootDirection = -1;
                        player.shootShuriken(shootDirection);
                    }
                }

            }
        }
        //Update Score
        hud.updateScore(currentScore);
        //Updating game world
        player.update(delta);
        ground.update(delta);
        for (Enemy enemy : enemies) {
            enemy.update(delta);
        }
        //Calculating Enemy Spawn Time
        ENEMYSPAWNTIME = (1500.0f + (1000.0f / waveGeneratorTimer)) / 1000;
        //Enemy Generation
        generateEnemies();
        //Collision Checking
        //Player-Ground
        if (Intersector.overlaps(player.getHitBox(), ground.getBoundingRectangle())) {
            player.isOnGround = true;
            player.velocity.y = 0;
            player.position.y = ground.getY() + ground.getHeight();
        }


        //Shurikne-Enemies
        for (Enemy enemy : enemies) {
            for (Shuriken shuriken : player.getShurikenArray()) {
                //Shuriken-Bat
                if (enemy instanceof Bat) {
                    if (Intersector.overlaps(((Bat) enemy).getHitBox(), shuriken.getBoundingRectangle())) {
                        currentScore++;
                        //Create and Play Boom Effect
                        createBoomEffect(shuriken.getX(), shuriken.getY());
                        //Remove from active enemies and return to pool
                        enemy.isAlive = false;

                        player.getShurikenArray().removeValue(shuriken, true);

                        player.shotsHited++;

                    }
                } else if (enemy instanceof EvilNinja) {
                    //Shuriken-EVILNINJA
                    if (Intersector.overlaps(((EvilNinja) enemy).getHitBox(), shuriken.getBoundingRectangle())) {
                        currentScore++;
                        //Play boom effect
                        createBoomEffect(shuriken.getX(), shuriken.getY());
                        //Remove from active enemies and return to pool
                        enemy.isAlive = false;
                        player.getShurikenArray().removeValue(shuriken, true);

                        player.shotsHited++;
                    }
                }
            }
            //Player-Enemy
            if (enemy instanceof Bat) {
                //Player-Bat
                if (Intersector.overlaps(((Bat) enemy).getHitBox(), player.getHitBox())) {
                    if (!((Bat) enemy).isClaimed()) {
                        createBloodSplatEffect(enemy.getPosition().x, enemy.getPosition().y);
                        //player.hitOnce = true;
                        ((Bat) enemy).claimed = true;
                    }
                    //Get Enemy Direction
                    if (((Bat) enemy).getDirection() > Constants.DIRECTION_LEFT) {
                        //HIT FROM RIGHT DIRECTION
                        player.rotationClockWise = false;
                    }
                    //if player has health ,player blinks if not player dies
                    if (hud.healthBar.isPlayerHasNoHealth()) {
                        player.alive = false;
                    } else {
                        player.blinker.setBlinking(true);
                    }
                }
            }

            //Player-Evil Ninja Shuriken
            if (enemy instanceof EvilNinja) {
                for (Shuriken shuriken : ((EvilNinja) enemy).getShurikenArray()) {
                    if (Intersector.overlaps(shuriken.getBoundingRectangle(), player.getHitBox())) {
                        ((EvilNinja) enemy).getShurikenArray().removeValue(shuriken, true);
                        createBloodSplatEffect(shuriken.getX(), shuriken.getY());
                        hud.healthBar.hitByEnemy();
                        //if player has health player blinks if not cut off player's head.
                        if (hud.healthBar.isPlayerHasNoHealth()) {
                            player.alive = false;
                            player.setHeadFallDirection(shuriken.direction);
                            player.hitByShuriken = true;
                        } else {
                            player.blinker.setBlinking(true);
                        }

                    }
                }
            }
            //Enemy-Ground
            if (enemy.getPosition().y < 16) {
                //Reset enemy position to ground
                enemy.velocity.y = 0;
                enemy.position.y = 16;
                enemy.isOnGround = true;
            }

            //Return enemies to Pool if out of screen
            if (!enemy.isAlive) {
                enemies.removeValue(enemy, true);
                enemyPool.free(enemy);
            }
        }

        //Game Over Conditions
        if (player.getPosition().y < -32 || player.getPlayerHead().getY() < -32) {
            currentGameState = GameState.GAMEOVER;
        }

        //Updating Camera
        camera.update();
    }


    private void updateGameOver(float delta) {
        //If game over not shown
        if (!gameOverShown) {
            //Calcaulate accuracy
            accuracy = player.shotsHited/player.shotsFired*100;
            //Stop Music
            music.stop();
            //Show Game Over UI
            hud.showGameOver(currentScore,accuracy);
            //Submit Score to Leaderboard
            game.gameServiceClient.submitToLeaderboard(Constants.LEADERBOARD_ID, currentScore, null);

            gameOverShown = true;
        }
    }

    /**
     * Plays blast particle effect
     **/
    private void createBoomEffect(float x, float y) {
        ParticleEffectPool.PooledEffect newEffect = boomParticleEffectPool.obtain();
        newEffect.setPosition(x, y);
        newEffect.start();
        pooledEffects.add(newEffect);
    }

    /**
     * Plays blood particle effect
     */
    private void createBloodEffect(float x, float y) {
        ParticleEffectPool.PooledEffect newEffect = bloodParticleEffectPool.obtain();
        newEffect.setPosition(x + player.getHitBox().width / 2, y + player.getHitBox().height / 2);
        newEffect.start();
        pooledEffects.add(newEffect);
    }

    /**
     * Plays blood Splat particle effect
     */
    private void createBloodSplatEffect(float x, float y) {
        ParticleEffectPool.PooledEffect newEffect = bloodSplatParticleEffectPool.obtain();
        newEffect.setPosition(x, y + player.getHitBox().height / 2);
        newEffect.start();
        pooledEffects.add(newEffect);
    }


    /**
     * Generates enemy based on level
     */
    private void generateEnemies() {
        if (enemySpawner > ENEMYSPAWNTIME && player.alive) {
            //Random Direction
            int newDirection = MathUtils.random(Constants.DIRECTION_LEFT, Constants.DIRECTION_RIGHT);
            //TODO:- ENEMY GENERATION DIRECTION BUG
            Enemy enemy = enemyPool.obtain();
            //Creating with enemy with new direction,position,flip
            enemy.setDirection(newDirection);
            if (newDirection > Constants.DIRECTION_LEFT && !enemy.isFlipped()
                    || newDirection < Constants.DIRECTION_RIGHT && enemy.isFlipped()) {
                enemy.flip(true, false);
            } else {
                enemy.flip(false, false);
            }

            if (enemy instanceof Bat) {
                enemy.setPosition(Constants.WORLD_WIDTH * newDirection, MathUtils.random(116, 216));
            } else {
                enemy.setPosition(Constants.WORLD_WIDTH * newDirection, 16);
            }
            enemies.add(enemy);
            enemySpawner = 0;
        }
    }

    @Override
    public void resize(int width, int height) {
        hud.stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {
        super.pause();
    }

    @Override
    public void resume() {
        super.resume();
    }

    @Override
    public void hide() {
        super.hide();
    }

    @Override
    public void dispose() {
        super.dispose();
        hud.stage.dispose();
    }
}
