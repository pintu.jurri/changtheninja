package tk.indieme.changtheninja.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.FitViewport;
import tk.indieme.changtheninja.ChangTheNinjaGame;
import tk.indieme.changtheninja.Constants;

public class ControlSelectionScreen extends AbstractScreen {
    private Stage stage;

    public ControlSelectionScreen(ChangTheNinjaGame game) {
        super(game);
        stage = new Stage(new FitViewport(Constants.WORLD_WIDTH, Constants.WORLD_HEIGHT));
        Gdx.input.setInputProcessor(stage);

    }

    @Override
    public void show() {
        //Controls bg UI
        //Mobile
        ImageButton.ImageButtonStyle btnMobileStyle = new ImageButton.ImageButtonStyle();
        btnMobileStyle.imageDown = new TextureRegionDrawable(game.assetLoader.mobilebgUI);
        btnMobileStyle.imageUp = new TextureRegionDrawable(game.assetLoader.mobilebgUI);

        final ImageButton btnMobileControl = new ImageButton(btnMobileStyle);
        btnMobileControl.setPosition(Constants.WORLD_WIDTH / 2, 0);
        //Desktop
        ImageButton.ImageButtonStyle btnDesktopStyle = new ImageButton.ImageButtonStyle();
        btnDesktopStyle.imageUp = new TextureRegionDrawable(game.assetLoader.webglbgUI);
        btnDesktopStyle.imageDown = new TextureRegionDrawable(game.assetLoader.webglbgUI);

        ImageButton btnDesktopControl = new ImageButton(btnDesktopStyle);
        btnDesktopControl.setPosition(0, 0);

        //Controls
        //Mobile
        Image imgMobileControl = new Image(game.assetLoader.uiAtlas.findRegion("mobilecontrol"));
        //imgMobileControl.setPosition(Constants.WORLD_WIDTH *3/4, Constants.WORLD_HEIGHT / 2);

        //WEB GL control
        Image imgWebglControl = new Image(game.assetLoader.uiAtlas.findRegion("webglcontroljump"));
        //imgWebglControl.setPosition(Constants.WORLD_WIDTH/4, Constants.WORLD_HEIGHT / 2);

        //Label
        //Mobile
        Image imgMobileLabel = new Image(game.assetLoader.uiAtlas.findRegion("labelMobile"));
        //Desktop
        Image imgDesktopLabel = new Image(game.assetLoader.uiAtlas.findRegion("labelDesktop"));

        VerticalGroup desktopGroup = new VerticalGroup();
        desktopGroup.space(10);
        desktopGroup.addActor(imgDesktopLabel);
        desktopGroup.addActor(imgWebglControl);
        desktopGroup.setPosition(Constants.WORLD_WIDTH / 2 - 128, Constants.WORLD_HEIGHT / 2);

        VerticalGroup mobileGroup = new VerticalGroup();
        mobileGroup.space(10);
        mobileGroup.addActor(imgMobileLabel);
        mobileGroup.addActor(imgMobileControl);
        mobileGroup.setPosition((Constants.WORLD_WIDTH * 3 / 4), Constants.WORLD_HEIGHT / 2);
        //Adding components to stage
        stage.addActor(btnMobileControl);
        stage.addActor(btnDesktopControl);
        stage.addActor(desktopGroup);
        stage.addActor(mobileGroup);

        btnMobileControl.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                //set mobile controls
                game.gamePref.putString("controls","mobile");
                game.setScreen(new PlayScreen(game));
            }
        });

        btnDesktopControl.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                //set for desktop controls
                game.gamePref.putString("controls","desktop");
                game.setScreen(new PlayScreen(game));

            }
        });
    }

    @Override
    public void render(float delta) {
        stage.act();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {
        super.pause();
    }

    @Override
    public void resume() {
        super.resume();
    }

    @Override
    public void hide() {
        super.hide();
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
