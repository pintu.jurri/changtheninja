package tk.indieme.changtheninja.screens;

import com.badlogic.gdx.Screen;
import tk.indieme.changtheninja.ChangTheNinjaGame;

public class AbstractScreen implements Screen {
    protected ChangTheNinjaGame game;

    public AbstractScreen(ChangTheNinjaGame game) {
        this.game = game;
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
