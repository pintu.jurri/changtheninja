package tk.indieme.changtheninja.screens;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FitViewport;
import de.golfgl.gdxgamesvcs.GameServiceException;
import de.golfgl.gdxgamesvcs.IGameServiceClient;
import de.golfgl.gdxgamesvcs.leaderboard.IFetchLeaderBoardEntriesResponseListener;
import de.golfgl.gdxgamesvcs.leaderboard.ILeaderBoardEntry;
import tk.indieme.changtheninja.ChangTheNinjaGame;
import tk.indieme.changtheninja.Constants;

public class StartScreen extends AbstractScreen {
    private Stage stage;

    public StartScreen(ChangTheNinjaGame game) {
        super(game);
        stage = new Stage(new FitViewport(Constants.WORLD_WIDTH, Constants.WORLD_HEIGHT));
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void show() {
        //Fade-In Transition
        stage.getRoot().getColor().a = 0;
        stage.getRoot().addAction(Actions.fadeIn(0.5f));
        //Creating UI

        //Label
        Label.LabelStyle labelStyle = new Label.LabelStyle();
        labelStyle.font = game.assetLoader.font16;

        //Background Image
        Image bg = new Image(game.assetLoader.bgUI);


        //Title Image
        Image title = new Image(game.assetLoader.uiAtlas.findRegion("title"));


        //Normal Button Style
        TextButton.TextButtonStyle textButtonStyle = new TextButton.TextButtonStyle();
        textButtonStyle.down = new TextureRegionDrawable(game.assetLoader.uiAtlas.findRegion("normalbtn", 2));
        textButtonStyle.up = new TextureRegionDrawable(game.assetLoader.uiAtlas.findRegion("normalbtn", 1));
        textButtonStyle.font = game.assetLoader.font16;
        textButtonStyle.unpressedOffsetY = 4;
        textButtonStyle.pressedOffsetY = -2;

        //Play Button
        final TextButton btnPlay = new TextButton("START", textButtonStyle);
        //Leaderboard Button
        final TextButton btnLeaderboard = new TextButton("LEADERBOARD", textButtonStyle);

        final TextButton btnAchievements = new TextButton("ACHIEVEMENT", textButtonStyle);

        //Quit Button
        final TextButton btnQuit = new TextButton("QUIT", textButtonStyle);

        //Creating Table for UI
        Table table = new Table();
        table.setFillParent(true);
        //Adding components to table
        table.add(title).spaceBottom(20);
        table.row();
        table.add(btnPlay).spaceBottom(5);
        table.row();
        table.add(btnLeaderboard).spaceBottom(5);
        table.row();
        table.add(btnQuit);
        //setting stage
        stage.addActor(bg);
        stage.addActor(table);

        //Button Listeners
        btnPlay.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {

                if (Gdx.app.getType().equals(Application.ApplicationType.WebGL) ||
                        Gdx.app.getType().equals(Application.ApplicationType.Desktop)) {
                    game.setScreen(new ControlSelectionScreen(game));
                    dispose();
                } else {
                    switchScreen(game, new PlayScreen(game));

                }
            }
        });
        btnLeaderboard.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {

                try {

                    game.gameServiceClient.showLeaderboards(Constants.LEADERBOARD_ID);
                } catch (GameServiceException e) {
                    e.printStackTrace();
                }

            }
        });
        btnQuit.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                Gdx.app.exit();
            }
        });
    }

    @Override
    public void render(float delta) {
        stage.act();
        stage.draw();

    }

    private void switchScreen(final Game game, final Screen newScreen) {
        //Fade Out Transition
        stage.getRoot().setColor(0, 0, 0, 0);
        stage.getRoot().getColor().a = 1;
        SequenceAction sequenceAction = new SequenceAction();
        sequenceAction.addAction(Actions.fadeOut(0.5f));
        sequenceAction.addAction(Actions.run(new Runnable() {
            @Override
            public void run() {
                game.setScreen(newScreen);
                dispose();
            }
        }));
        stage.getRoot().addAction(sequenceAction);
    }

    @Override
    public void resize(int width, int height) {

        stage.getViewport().update(width, height);
    }

    @Override
    public void pause() {
        super.pause();
    }

    @Override
    public void resume() {
        super.resume();
    }

    @Override
    public void hide() {
        super.hide();
    }

    @Override
    public void dispose() {
        super.dispose();
        stage.dispose();
    }
}
