package tk.indieme.changtheninja;

public interface StringFormatter {
    String getFormattedString(int value);
}
