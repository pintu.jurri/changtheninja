package tk.indieme.changtheninja;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.viewport.FitViewport;
import tk.indieme.changtheninja.models.HealthBar;
import tk.indieme.changtheninja.screens.PlayScreen;

public class Hud implements Disposable {
    public Stage stage;
    private ChangTheNinjaGame game;
    //HUD
    private Label scoreLabel, lifeLabel;
    private Image scoreWindow;
    public HealthBar healthBar;
    public HorizontalGroup healthBarGroup;

    //GameOver
    private Label gameOverScoreLabel, accuracyLabel;
    private Table gameOverTable;
    private Image gameover, window;
    private HorizontalGroup btnGroup;
    public TextButton btnContinue, btnRestart;
    //Controls Tutorial
    private Image imgShoot, imgJump, webglControlJump, webglControlShoot;
    private Label labelTapJump, labelSwipeShoot;
    private VerticalGroup leftGroup, rightGroup;

    public Hud(ChangTheNinjaGame game) {
        this.game = game;
        stage = new Stage(new FitViewport(Constants.WORLD_WIDTH, Constants.WORLD_HEIGHT), game.batch);

        Gdx.input.setInputProcessor(stage);

    }

    public void createHUD() {

        //Gameover Image
        gameover = new Image(game.assetLoader.uiAtlas.findRegion("gameover"));
        gameover.setPosition(Constants.WORLD_WIDTH / 2 - gameover.getWidth() / 2, Constants.WORLD_HEIGHT);
        //Window Image
        window = new Image(game.assetLoader.uiAtlas.findRegion("window"));
        window.setPosition(Constants.WORLD_WIDTH / 2 - window.getWidth() / 2, Constants.WORLD_HEIGHT / 2);
        //Gameover table
        gameOverTable = new Table();
        gameOverTable.setFillParent(true);

        //Normal Button Style
        TextButton.TextButtonStyle textButtonStyle = new TextButton.TextButtonStyle();
        textButtonStyle.down = new TextureRegionDrawable(game.assetLoader.uiAtlas.findRegion("normalbtn", 2));
        textButtonStyle.up = new TextureRegionDrawable(game.assetLoader.uiAtlas.findRegion("normalbtn", 1));
        textButtonStyle.font = game.assetLoader.font16;
        textButtonStyle.unpressedOffsetY = 4;
        textButtonStyle.pressedOffsetY = -2;

        //Continue Button
        btnContinue = new TextButton("CONTINUE", textButtonStyle);
        btnRestart = new TextButton("RESTART", textButtonStyle);
        //Horizontal Group for button
        btnGroup = new HorizontalGroup();
        btnGroup.space(20);
        btnGroup.addActor(btnContinue);
        btnGroup.addActor(btnRestart);
        btnGroup.setPosition(Constants.WORLD_WIDTH / 2 - window.getWidth() / 2, -64);
        //Score LabelStyle
        Label.LabelStyle labelStyle = new Label.LabelStyle();
        labelStyle.font = game.assetLoader.font16;
        labelStyle.fontColor = Color.BLACK;
        //SCore Window
        scoreWindow = new Image(game.assetLoader.uiAtlas.findRegion("scorewindow"));
        scoreWindow.setPosition(0, Constants.WORLD_HEIGHT - 48);
        //Score label
        String defaultFormattedScore = game.stringFormatter.getFormattedString(0);
        scoreLabel = new Label(defaultFormattedScore, labelStyle);
        scoreLabel.setPosition(24, Constants.WORLD_HEIGHT - 32);
        //Accuracy label
        accuracyLabel = new Label(defaultFormattedScore, labelStyle);
        accuracyLabel.setPosition(Constants.WORLD_WIDTH/2-accuracyLabel.getWidth(), Constants.WORLD_HEIGHT/2 - 64);
        //Life Label
        lifeLabel = new Label("Life", labelStyle);
        //game over score label
        gameOverScoreLabel = new Label(defaultFormattedScore, labelStyle);
        gameOverScoreLabel.setPosition(Constants.WORLD_WIDTH / 2 - scoreLabel.getWidth(), Constants.WORLD_HEIGHT / 2);
        //HealthBar
        healthBar = new HealthBar(Constants.WORLD_WIDTH / 2 - 32, Constants.WORLD_HEIGHT - 22, game);

        //Adding health bar label and healthbar to group
        //Image Shoot and Jump
        imgJump = new Image(game.assetLoader.uiAtlas.findRegion("jump"));
        imgShoot = new Image(game.assetLoader.uiAtlas.findRegion("shoot"));

        //Label Jump and Shoot
        labelTapJump = new Label("<Tap To Jump>", labelStyle);
        labelSwipeShoot = new Label("<Swipe To Shoot>", labelStyle);
        //Image Controls
        webglControlJump = new Image(game.assetLoader.uiAtlas.findRegion("webglcontroljump"));
        webglControlShoot = new Image(game.assetLoader.uiAtlas.findRegion("webglcontrolshoot"));

        leftGroup = new VerticalGroup();
        rightGroup = new VerticalGroup();


        //Table for HUD elements
        Table table = new Table();
        table.setFillParent(true);
        table.top();
        table.addActor(scoreWindow);
        table.addActor(scoreLabel);
        table.add(lifeLabel).spaceRight(128);
        table.add(healthBar);

        //Add Components to UI
        stage.addActor(table);

        //Set Listener for game over Screen
        btnRestart.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if (Gdx.app.getType().equals(Application.ApplicationType.WebGL)) {
                    game.setScreen(new PlayScreen(game));
                    dispose();
                } else {
                    switchScreen(game, new PlayScreen(game));
                }


            }
        });

        //Creating animation for gameover title
        MoveToAction actionMove1 = Actions.action(MoveToAction.class);
        actionMove1.setPosition(Constants.WORLD_WIDTH / 2 - gameover.getWidth() / 2, Constants.GAMEOVER_POSITION);
        actionMove1.setInterpolation(Interpolation.bounceOut);
        actionMove1.setDuration(0.5f);

        gameover.addAction(actionMove1);

        MoveToAction actionMove3 = Actions.action(MoveToAction.class);

        actionMove3.setPosition(Constants.WORLD_WIDTH / 2 - window.getWidth() / 2, Constants.GROUP_POSITION);
        actionMove3.setDuration(0.25f);

        btnGroup.addAction(actionMove3);
    }

    public void showGameOver(int currentScore, float currentAccuracy) {
        //Update Game Over Score
        String formattedCurrentScore = game.stringFormatter.getFormattedString(currentScore);
        gameOverScoreLabel.setText(formattedCurrentScore);
        String formattedCurrentAccuracy = game.stringFormatter.getFormattedString((int)currentAccuracy);
        accuracyLabel.setText(formattedCurrentAccuracy);

        //Hide Score Label
        scoreLabel.setVisible(false);
        //Adding Components to table
        gameOverTable.add(gameover).expandX();
        gameOverTable.row();
        gameOverTable.add(window).spaceBottom(16);
        gameOverTable.row();
        gameOverTable.add(btnGroup);

        stage.addActor(gameOverTable);
        stage.addActor(gameOverScoreLabel);
        stage.addActor(accuracyLabel);

    }

    public void showMobileControls() {
        //Creating tutorial UI

        leftGroup.space(10);
        leftGroup.addActor(imgJump);
        leftGroup.addActor(labelTapJump);
        leftGroup.setPosition(Constants.WORLD_WIDTH / 2 - 128, Constants.WORLD_HEIGHT / 2);

        rightGroup.space(10);
        rightGroup.addActor(imgShoot);
        rightGroup.addActor(labelSwipeShoot);
        rightGroup.setPosition(Constants.WORLD_WIDTH * 3 / 4, Constants.WORLD_HEIGHT / 2);

        stage.addActor(leftGroup);
        stage.addActor(rightGroup);
    }

    public void showDesktopControls() {
        //Creating tutorial UI
        leftGroup = new VerticalGroup();
        leftGroup.space(10);
        leftGroup.addActor(imgJump);
        leftGroup.addActor(webglControlJump);
        leftGroup.setPosition(Constants.WORLD_WIDTH / 2 - 128, Constants.WORLD_HEIGHT / 2);

        rightGroup = new VerticalGroup();
        rightGroup.space(10);
        rightGroup.addActor(imgShoot);
        rightGroup.addActor(webglControlShoot);
        rightGroup.setPosition(Constants.WORLD_WIDTH * 3 / 4, Constants.WORLD_HEIGHT / 2);

        stage.addActor(leftGroup);
        stage.addActor(rightGroup);
    }

    private void removeGameOver() {
        gameOverTable.remove();
    }

    public void removeControls() {
        leftGroup.clear();
        rightGroup.clear();
        leftGroup.remove();
        rightGroup.remove();

    }

    private void switchScreen(final Game game, final Screen newScreen) {
        //Fade Out Transition
        stage.getRoot().setColor(0, 0, 0, 0);
        stage.getRoot().getColor().a = 1;
        SequenceAction sequenceAction = new SequenceAction();
        sequenceAction.addAction(Actions.fadeOut(0.5f));
        sequenceAction.addAction(Actions.run(new Runnable() {
            @Override
            public void run() {
                game.setScreen(newScreen);
                dispose();
            }
        }));
        stage.getRoot().addAction(sequenceAction);
        removeGameOver();
    }

    public void updateScore(int score) {
        String formattedScore = game.stringFormatter.getFormattedString(score);
        scoreLabel.setText(formattedScore);
        // scoreLabel.setText(String.format(Locale.ENGLISH, "KILLS: %d", score));

    }


    @Override
    public void dispose() {
        stage.dispose();
    }
}
