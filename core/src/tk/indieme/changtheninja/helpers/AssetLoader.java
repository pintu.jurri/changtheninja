package tk.indieme.changtheninja.helpers;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Disposable;

public class AssetLoader implements Disposable {
    private AssetManager assetManager;
    //In-Game Objects
    public Texture plain_ground, bullet;
    public TextureRegion background01;
    public TextureAtlas player, enemies;
    public Texture shuriken;
    //Ui
    public Texture title;
    public TextureAtlas uiAtlas;
    public TextureRegion bgUI, webglbgUI, mobilebgUI;
    //fonts
    public BitmapFont font32, font16;
    //Background
    public Texture bg_01, bg_02;
    //Audio
    public Sound sfxJump, sfxShoot;
    public Music mainMusic;
    //Particle Effects
    public ParticleEffect blastEffect, bloodEffect, bloodSplatEffect;

    public void init() {
        if (assetManager == null) {
            assetManager = new AssetManager();
        }
        //loading in-game objects
        assetManager.load("backgrounds/background_01.png", Texture.class);
        assetManager.load("backgrounds/ground_02.png", Texture.class);
        assetManager.load("player/player.atlas", TextureAtlas.class);
        assetManager.load("objects/shuriken.png", Texture.class);
        assetManager.load("enemies/enemies.atlas", TextureAtlas.class);
        assetManager.load("fx/boom.p", ParticleEffect.class);
        assetManager.load("fx/blood.p", ParticleEffect.class);
        assetManager.load("fx/bloodSplat.p", ParticleEffect.class);
        //loading UI
        assetManager.load("ui/ui.atlas", TextureAtlas.class);
        assetManager.load("ui/bg.png", Texture.class);
        assetManager.load("ui/controlsbg1.png", Texture.class);
        assetManager.load("ui/controlsbg2.png", Texture.class);
        //loading effects
        // loading fonts
        assetManager.load("fonts/font.fnt", BitmapFont.class);
        assetManager.load("fonts/font16.fnt", BitmapFont.class);

        //loading Sound
        assetManager.load("sfx/jump.wav", Sound.class);
        assetManager.load("sfx/shoot.wav", Sound.class);
        assetManager.load("sfx/music.wav", Music.class);

        assetManager.finishLoading();

        load();
    }

    public void load() {
        //loading objects
        background01 = new TextureRegion(assetManager.get("backgrounds/background_01.png", Texture.class));
        plain_ground = assetManager.get("backgrounds/ground_02.png", Texture.class);
        enemies = assetManager.get("enemies/enemies.atlas", TextureAtlas.class);
        player = assetManager.get("player/player.atlas", TextureAtlas.class);
        shuriken = assetManager.get("objects/shuriken.png", Texture.class);
        //loading UI
        uiAtlas = assetManager.get("ui/ui.atlas", TextureAtlas.class);
        bgUI = new TextureRegion(assetManager.get("ui/bg.png", Texture.class));
        webglbgUI = new TextureRegion(assetManager.get("ui/controlsbg1.png", Texture.class));
        mobilebgUI = new TextureRegion(assetManager.get("ui/controlsbg2.png", Texture.class));
        //loading particle effects
        blastEffect = assetManager.get("fx/boom.p", ParticleEffect.class);
        bloodEffect = assetManager.get("fx/blood.p", ParticleEffect.class);
        bloodSplatEffect = assetManager.get("fx/bloodSplat.p", ParticleEffect.class);
        //loading fonts
        font32 = assetManager.get("fonts/font.fnt", BitmapFont.class);
        font16 = assetManager.get("fonts/font16.fnt", BitmapFont.class);
        //loading sounds
        sfxJump = assetManager.get("sfx/jump.wav", Sound.class);
        sfxShoot = assetManager.get("sfx/shoot.wav", Sound.class);
        mainMusic = assetManager.get("sfx/music.wav", Music.class);


    }

    public AssetManager getAssetManager() {
        return assetManager;
    }

    @Override
    public void dispose() {
        assetManager.dispose();
    }
}

