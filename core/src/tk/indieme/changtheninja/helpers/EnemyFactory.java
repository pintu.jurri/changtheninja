package tk.indieme.changtheninja.helpers;

import com.badlogic.gdx.math.MathUtils;
import tk.indieme.changtheninja.ChangTheNinjaGame;
import tk.indieme.changtheninja.EnemyType;
import tk.indieme.changtheninja.models.Bat;
import tk.indieme.changtheninja.models.Enemy;
import tk.indieme.changtheninja.models.EvilNinja;

public class EnemyFactory {
    public static Enemy getEnemy(float x, float y, int direction, final ChangTheNinjaGame game, EnemyType compareTo) {
        Enemy enemy = null;
        switch (compareTo) {
            case EVILNINJA:
                enemy = new EvilNinja(x, y, direction, game);
                break;
            case BAT:
                enemy = new Bat(x, MathUtils.random(y+100,y+200), direction, game);
                break;
            default:
                enemy = new EvilNinja(x, y, direction, game);

        }
        return enemy;

    }
}
