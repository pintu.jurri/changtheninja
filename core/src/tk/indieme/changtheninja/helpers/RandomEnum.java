package tk.indieme.changtheninja.helpers;

import java.util.Random;
/*
* This is parameterized class which takes enum class as input
* and returns random enum value.
* */

public class RandomEnum<E extends Enum> {
    private static final Random RND = new Random();
    private final E[] values;   //enum constants array

    //initializes enum array with enum constants
    public RandomEnum(Class<E> token) {
        values = token.getEnumConstants();
    }

    //return random value from enum constants array
    public E random() {
        return values[RND.nextInt(values.length)];
    }

}
