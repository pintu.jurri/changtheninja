package tk.indieme.changtheninja.helpers;

public interface PlayServices {
    public void signIn();

    public void signOut();

    public void rateGame();

    void shareGame(int score);

    public void submitScore(String leaderboard, int highScore);

    public void showScore();

    public boolean isSignedIn();

    void showOrLoadInterstital();
}
