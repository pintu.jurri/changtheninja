package tk.indieme.changtheninja.models;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import tk.indieme.changtheninja.ChangTheNinjaGame;

public class HealthBar extends Actor {
    //Position
    private Vector2 position;
    //Scale
    private float maxHealthBarX, maxHealthBarY = 16;
    //Health minus value
    private static final float HEALTH_MINUS = 150;
    private boolean hitByEnemies;
    private float previousHealth;
    private boolean playerNoHealth;
    //Textures
    private TextureRegion healtBar, healthBarOverlap;

    public HealthBar(float x, float y, ChangTheNinjaGame game) {

        healtBar = game.assetLoader.uiAtlas.findRegion("healthbar");
        healthBarOverlap = game.assetLoader.uiAtlas.findRegion("healthbarOverlap");

        maxHealthBarX = healtBar.getRegionWidth();
        position = new Vector2(x, y);
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        previousHealth = maxHealthBarX;
        //Reduces health if hit by player
        if (hitByEnemies && maxHealthBarX > 0) {
            maxHealthBarX = MathUtils.lerp(maxHealthBarX, maxHealthBarX - HEALTH_MINUS, 0.1f);
        }

        if (maxHealthBarX < previousHealth) {

            hitByEnemies = false;
        }
        if (maxHealthBarX <= 0) {
            maxHealthBarX = 0;
            playerNoHealth = true;

        }
        setPosition(position.x,position.y);
    }

    @Override
    public void setX(float x) {
        position.x = x;
    }

    public void hitByEnemy() {
        hitByEnemies = true;
    }

    public boolean isPlayerHasNoHealth() {
        //false: if player has health
        //true : if player has no health
        return playerNoHealth;
    }


    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        batch.draw(healthBarOverlap, position.x, position.y);
        batch.draw(healtBar, position.x, position.y, maxHealthBarX, maxHealthBarY);
    }
}
