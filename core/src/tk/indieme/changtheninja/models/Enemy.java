package tk.indieme.changtheninja.models;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Shape2D;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool;
import tk.indieme.changtheninja.ChangTheNinjaGame;
import tk.indieme.changtheninja.Constants;

public abstract class Enemy implements Pool.Poolable {
    //Physics
    public Vector2 position, velocity, gravity;
    protected float MAX_GRAVITY = -500;
    protected float JUMP_HEIGHT = 350;
    public boolean isOnGround = true;
    public boolean isAlive;
    protected int direction = 0;
    protected ChangTheNinjaGame game;

    public Enemy(float x, float y, int direction, final ChangTheNinjaGame game) {
        this.game = game;
        position = new Vector2(x, y);
        gravity = new Vector2(0, -580);
        velocity = new Vector2(0, 0);
        this.direction = direction;
        isAlive = true;
    }

    public Vector2 getPosition() {
        return position;
    }

    public int getDirection() {
        //0-means left,1-means right
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;

    }

    public void setPosition(float x, float y) {
        position.set(x, y);
    }
    public abstract boolean isFlipped();

    public abstract void flip(boolean flipX, boolean flipY);

    public abstract Shape2D getHitBox();

    public abstract void update(float delta);

    public abstract void draw(SpriteBatch batch);

    public abstract void drawDebug(ShapeRenderer shapeRenderer);

}
