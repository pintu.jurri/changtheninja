package tk.indieme.changtheninja.models;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import tk.indieme.changtheninja.ChangTheNinjaGame;
import tk.indieme.changtheninja.Constants;

public class Bat extends Enemy {
    //Physics
    private static final float FLY_SPEED = 75;
    //Textures,Animation
    private Animation<TextureRegion> flyAnim;
    private float stateTimer;
    public boolean claimed;
    //Hitbox
    private Circle hitBox;
    private int RADIUS = 8;
    private int WIDTH = 16, HEIGHT = 16;

    public Bat(float x, float y, int direction, ChangTheNinjaGame game) {
        super(x, y, direction, game);
        Array<TextureAtlas.AtlasRegion> frames = game.assetLoader.enemies.findRegions("evilbat");
        flyAnim = new Animation<TextureRegion>(1 / 15f, frames, Animation.PlayMode.LOOP);
        hitBox = new Circle(x + WIDTH / 2, y + HEIGHT / 2, RADIUS);
    }

    //Getters
    public Circle getHitBox() {
        return hitBox;
    }

    public boolean isClaimed(){
        return  claimed;
    }

    @Override
    public void flip(boolean flipX, boolean flipY) {
        TextureRegion[] frames = flyAnim.getKeyFrames();
        for (TextureRegion frame : frames) {
            frame.flip(flipX, flipY);
        }
    }

    @Override
    public boolean isFlipped() {
        return flyAnim.getKeyFrames()[0].isFlipX();
    }

    public void update(float delta) {
        stateTimer += delta;
        if (direction > 0) {
            velocity.x = -FLY_SPEED;
        } else {
            velocity.x = FLY_SPEED;
        }

        position.mulAdd(velocity, delta);
        //Updates HitBox
        hitBox.setPosition(position.x + WIDTH / 2, position.y + HEIGHT / 2);
        //Check if out of screen
        if (position.x > Constants.ENEMY_SPAWNRIGHTX || position.x < Constants.ENEMY_SPAWNLEFTX) {
            isAlive = false;
        }
    }

    @Override
    public void reset() {
        isAlive = true;
        claimed = false;
        if (direction < Constants.DIRECTION_RIGHT) {
            direction = Constants.DIRECTION_LEFT;
            //Set position to LEFT
            position.set(Constants.ENEMY_SPAWNLEFTX, MathUtils.random(116, 216));
        } else {
            direction = Constants.DIRECTION_RIGHT;
            //Set position to RIGHT
            position.set(Constants.ENEMY_SPAWNRIGHTX, MathUtils.random(116, 216));
        }

    }

    public void draw(SpriteBatch batch) {
        TextureRegion region = null;
        region = flyAnim.getKeyFrame(stateTimer);
        batch.draw(region, position.x, position.y);
    }

    public void drawDebug(ShapeRenderer shapeRenderer) {
        shapeRenderer.circle(hitBox.x, hitBox.y, hitBox.radius);
    }
}
