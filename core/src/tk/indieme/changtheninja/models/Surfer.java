package tk.indieme.changtheninja.models;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Shape2D;
import tk.indieme.changtheninja.ChangTheNinjaGame;

public class Surfer extends Enemy {

    public Surfer(float x, float y, int direction, ChangTheNinjaGame game) {
        super(x, y, direction, game);
    }

    @Override
    public Shape2D getHitBox() {
        return null;
    }

    @Override
    public void flip(boolean flipX, boolean flipY) {

    }

    @Override
    public boolean isFlipped() {
        return false;
    }

    @Override
    public void update(float delta) {

    }

    @Override
    public void draw(SpriteBatch batch) {

    }

    @Override
    public void drawDebug(ShapeRenderer shapeRenderer) {

    }

    @Override
    public void reset() {

    }
}
