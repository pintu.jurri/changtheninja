package tk.indieme.changtheninja.models;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool;
import tk.indieme.changtheninja.ChangTheNinjaGame;
import tk.indieme.changtheninja.Constants;

public class Shuriken extends Sprite implements Pool.Poolable {
    private Texture texture;
    //Physics
    private Vector2 vel;
    private float rotation;
    public int direction = 0;
    public boolean isAlive;
    private float shurikenSpeed = 400;
    private float spawnX, spawnY;

    public Shuriken(float x, float y, ChangTheNinjaGame game) {

        texture = game.assetLoader.shuriken;
        isAlive = true;
        spawnX = x;
        spawnY = y;
        setBounds(x, y, texture.getWidth(), texture.getHeight());
        setOrigin(texture.getWidth() / 2, texture.getHeight() / 2);
        setRegion(texture);

        vel = new Vector2(0, 0);
        this.direction = direction;
        rotation = -MathUtils.random(10, 15);
    }

    public void setDirection(int shootDirection) {
        direction = shootDirection;
    }

    public void update(float delta) {

        vel.x = shurikenSpeed * direction * delta;
        //Rotate Shurikens
        rotate(rotation);
        setPosition(getX() + vel.x, getY());
        if (getX() > Constants.WORLD_WIDTH || getX() < 0) {
            isAlive = false;
        }


    }

    @Override
    public void reset() {
        isAlive = true;
        setPosition(0, Constants.WORLD_HEIGHT);


    }
    //Actions


    @Override
    public void draw(Batch batch) {
        if (isAlive) {
            super.draw(batch);
        }
    }
}
