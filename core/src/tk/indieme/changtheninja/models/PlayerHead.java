package tk.indieme.changtheninja.models;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import tk.indieme.changtheninja.ChangTheNinjaGame;

public class PlayerHead extends Sprite {
    //Texture
    private TextureRegion deathFrame;
    //Phyiscs
    private float MAX_GRAVITY = -500;
    private Vector2  velocity, gravity;
    private float rotationSpeed = 5;
    public int rotationDirection;
    public PlayerHead(float x, float y, ChangTheNinjaGame game) {
        deathFrame = game.assetLoader.player.findRegion("death", 2);

        velocity = new Vector2(0, 0);
        gravity = new Vector2(0, -580);

        setBounds(x, y, deathFrame.getRegionWidth(), deathFrame.getRegionHeight());
        setRegion(deathFrame);
    }

    public void update(float delta) {
        velocity.mulAdd(gravity, delta);
        velocity.x = 100;
        if (velocity.y < MAX_GRAVITY) {
            velocity.y = MAX_GRAVITY;
        }
        rotate(rotationSpeed*rotationDirection);
        setPosition(getX()+velocity.x*delta,getY()+velocity.y*delta);
    }

    public void draw(SpriteBatch batch) {
        super.draw(batch);
    }
}
