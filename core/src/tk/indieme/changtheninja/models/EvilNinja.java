package tk.indieme.changtheninja.models;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import tk.indieme.changtheninja.ChangTheNinjaGame;
import tk.indieme.changtheninja.Constants;

public class EvilNinja extends Enemy {
    //Textures,Animation
    private Animation<TextureRegion> runAnim;
    private TextureRegion attackFrame;
    private float stateTimer;

    //States
    private enum PlayerState {
        RUN, ATTACK
    }

    private PlayerState currentPlayerState;
    //AI
    private boolean jumpOnce = false;
    private float shurikenShootTimer;
    private Array<Shuriken> shurikenArray;
    //HitBox
    private Rectangle hitBox;
    //Pool Containing Shuriken
    private final Pool<Shuriken> shurikenPool = new Pool<Shuriken>() {
        @Override
        protected Shuriken newObject() {
            return new Shuriken(position.x + (8 * direction), position.y + 5, game);
        }
    };

    public EvilNinja(float x, float y, int direction, ChangTheNinjaGame game) {
        super(x, y, direction, game);
        Array<TextureAtlas.AtlasRegion> runFrames = game.assetLoader.enemies.findRegions("evilninja_run");
        attackFrame = runFrames.get(0);
        runAnim = new Animation<TextureRegion>(1 / 15f, runFrames, Animation.PlayMode.LOOP);

        hitBox = new Rectangle(x, y, 24, 24);

        currentPlayerState = PlayerState.RUN;

        shurikenArray = new Array<Shuriken>();

    }

    //Getters
    public Rectangle getHitBox() {
        return hitBox;
    }

    public Array<Shuriken> getShurikenArray() {
        return shurikenArray;
    }


    @Override
    public void flip(boolean flipX, boolean flipY) {
        TextureRegion[] frames = runAnim.getKeyFrames();
        //if not flipped then flip it
        for (TextureRegion frame : frames) {
            frame.flip(flipX, flipY);
        }

    }

    @Override
    public boolean isFlipped() {
        return runAnim.getKeyFrames()[0].isFlipX();
    }

    public void update(float delta) {
        stateTimer += delta;
        shurikenShootTimer += delta;
        if (isOnGround) {
            currentPlayerState = PlayerState.RUN;
        }

        velocity.mulAdd(gravity, delta);
        if (velocity.y < MAX_GRAVITY) {
            velocity.y = MAX_GRAVITY;
        }
        if (direction > 0) {
            //Right Direction
            velocity.x = -200;
        } else {
            //Left Direction
            velocity.x = 200;
        }
        //Jump Shoot Shuriken
        if (isOnGround && (!jumpOnce)) {
            if (direction < 1 && position.x >= Constants.WORLD_WIDTH / 4) {
                //Jump

                jump();
                velocity.x = 0;
                jumpOnce = true;

            } else if (direction > 0 && position.x < Constants.WORLD_WIDTH) {
                jump();
                velocity.x = 0;
                jumpOnce = true;
            }


        }
        if (!isOnGround) {
            shootShuriken();
            //Update Shurikens

        }
        for (Shuriken shuriken : shurikenArray) {
            shuriken.update(delta);
            if (!shuriken.isAlive) {
                shurikenArray.removeValue(shuriken, true);
                shurikenPool.free(shuriken);
            }
        }

        //Update Position
        position.mulAdd(velocity, delta);

        //update hitBox
        hitBox.setPosition(position.x, position.y);
        //Check if out of screen
        if (position.x > Constants.ENEMY_SPAWNRIGHTX || position.x < Constants.ENEMY_SPAWNLEFTX) {
            isAlive = false;
        }
    }

    /**
     * Callback method when the object is freed. It is automatically called by Pool.free()
     * Must reset every meaningful field of this bullet.
     */

    @Override
    public void reset() {
        shurikenArray.clear();
        jumpOnce = false;
        isAlive = true;
        if (direction < Constants.DIRECTION_RIGHT) {
            //Set position to LEFT
            position.set(Constants.ENEMY_SPAWNLEFTX, Constants.ENEMY_LANDSPAWNY);
        } else {
            //Set position to RIGHT
            position.set(Constants.ENEMY_SPAWNRIGHTX, Constants.ENEMY_LANDSPAWNY);
        }
        direction = Constants.DIRECTION_LEFT;
    }

    //Actions
    private void jump() {
        velocity.y = JUMP_HEIGHT;
        isOnGround = false;
    }

    private void shootShuriken() {
        currentPlayerState = PlayerState.ATTACK;
        int shootDirection = -1;
        if (direction < 1) {
            shootDirection = 1;
        }
        if (shurikenShootTimer > 0.25f) {
            Shuriken shuriken = shurikenPool.obtain();
            shuriken.setPosition(position.x, position.y);
            shuriken.setDirection(shootDirection);
            shurikenArray.add(shuriken);
            shurikenShootTimer = 0;
        }
    }


    public void draw(SpriteBatch batch) {
        TextureRegion region = null;
        switch (currentPlayerState) {
            case RUN:
                region = runAnim.getKeyFrame(stateTimer);
                break;
            case ATTACK:
                region = attackFrame;
                break;
        }

        for (Shuriken shuriken : shurikenArray) {
            shuriken.draw(batch);
        }
        if (isAlive) {
            batch.draw(region, position.x, position.y);
        }
    }

    public void drawDebug(ShapeRenderer shapeRenderer) {
        shapeRenderer.rect(hitBox.x, hitBox.y, hitBox.getWidth(), hitBox.getHeight());

    }
}
