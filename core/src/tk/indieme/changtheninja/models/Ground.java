package tk.indieme.changtheninja.models;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import tk.indieme.changtheninja.ChangTheNinjaGame;

public class Ground extends Sprite {
    //TExture
    private Texture texture;

    public Ground(float x, float y, ChangTheNinjaGame game) {
        texture = game.assetLoader.plain_ground;
        setBounds(x, y, texture.getWidth(), texture.getHeight());
        setRegion(texture);
    }

    public void update(float delta) {

    }

}
