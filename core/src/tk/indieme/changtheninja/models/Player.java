package tk.indieme.changtheninja.models;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import tk.indieme.changtheninja.ChangTheNinjaGame;
import tk.indieme.changtheninja.helpers.Blinker;

public class Player {


    private final ChangTheNinjaGame game;

    //Sounds
    private Sound sfxJump, sfxShoot;
    //Texture,Animation
    private PlayerHead playerHead;
    private TextureAtlas playerAtlas;
    private TextureRegion jumpFrame, deathFrame1;
    private Animation<TextureRegion> idleAnim, attackLeftAnim, attackRightAnim;
    public boolean alive = true;
    public boolean hitByShuriken = false;
    private float stateTimer;
    private float originX, originY, headOriginX, headOriginY;
    //Physics
    public Vector2 position, headPosition;
    public Vector2 velocity, headVelocity;
    private Vector2 gravity;
    private float MAX_GRAVITY = -500;
    private static final float DEATH_ANIMATION_SPEED = 75;
    public boolean isOnGround;
    private static final float JUMP_HEIGHT = 350;
    private int playerDirection = 0;
    private float rotation = 0;
    public boolean rotationClockWise;
    //Player blinker
    public Blinker blinker;


    //States
    private enum PlayerState {
        IDLE, ATTACK, JUMP
    }

    private boolean died;
    private PlayerState currentPlayerState;
    //HitBox
    private Rectangle hitBox;
    private int WIDTH = 18, HEIGHT = 24;
    //Shurikens
    private Array<Shuriken> shurikenArray;
    private float shurikenShootTimer;
    public boolean shoot;
    //To Calculate accuracy
    public float shotsFired,shotsHited;

    //Pool containing Shurikens
    private final Pool<Shuriken> shurikenPool = new Pool<Shuriken>() {
        @Override
        protected Shuriken newObject() {
            return new Shuriken(position.x + 8, position.y + 5, game);
        }
    };

    public Player(float x, float y, ChangTheNinjaGame game) {
        this.game = game;
        position = new Vector2(x, y);
        headPosition = new Vector2(x, y);
        headVelocity = new Vector2(0, 0);
        gravity = new Vector2(0, -580);
        velocity = new Vector2(0, 0);
        rotationClockWise = true;

        sfxJump = game.assetLoader.sfxJump;
        sfxShoot = game.assetLoader.sfxShoot;

        playerAtlas = game.assetLoader.player;
        jumpFrame = playerAtlas.findRegion("idle", 1);
        deathFrame1 = playerAtlas.findRegion("death", 1);
        playerHead = new PlayerHead(position.x, position.y, game);
        idleAnim = new Animation<TextureRegion>(0.15f, playerAtlas.findRegions("idle"), Animation.PlayMode.LOOP);
        attackLeftAnim = new Animation<TextureRegion>(0.15f, playerAtlas.findRegions("attackLeft"));
        attackRightAnim = new Animation<TextureRegion>(0.15f, playerAtlas.findRegions("attackRight"));
        currentPlayerState = PlayerState.IDLE;

        hitBox = new Rectangle(x, y, WIDTH, HEIGHT);
        originX = hitBox.x + WIDTH / 2;
        originY = hitBox.y + HEIGHT / 2;
        shurikenArray = new Array<Shuriken>();


        //Blinks player on hit
        blinker = new Blinker();
    }

    //Getters
    public Rectangle getHitBox() {
        return hitBox;
    }

    public Vector2 getPosition() {
        return position;
    }


    public PlayerHead getPlayerHead() {
        return playerHead;
    }

    public Array<Shuriken> getShurikenArray() {
        return shurikenArray;
    }

    public void setHeadFallDirection(int fallDirection) {
        playerHead.rotationDirection = fallDirection;
    }

    public void update(float delta) {
        if (isOnGround) {
            currentPlayerState = PlayerState.IDLE;
        } else {
            currentPlayerState = PlayerState.JUMP;
        }
        if (alive) {
            stateTimer += delta;
            shurikenShootTimer += delta;
            if (!isOnGround) {
                velocity.mulAdd(gravity, delta);
            }
            if (velocity.y < MAX_GRAVITY) {
                velocity.y = MAX_GRAVITY;
            }
        } else {

            if (!hitByShuriken) {
                //If hit by enemies but not by shuriken
                velocity.x = DEATH_ANIMATION_SPEED;
                //Clock Wise Rotation
                rotation += 480 * delta;
                if (rotation > 90) {
                    //   rotation = 90;
                }
            } else {
                //if hit by shuriken
                if (!died) {

                    playerHead.setPosition(position.x + 8, position.y + 8);
                    //Make head flown off
                    died = true;
                }
                playerHead.update(delta);
            }

            velocity.mulAdd(gravity, delta);
            if (velocity.y < MAX_GRAVITY) {
                velocity.y = MAX_GRAVITY;
            }


        }
        //Update Shurikens
        for (Shuriken shuriken : shurikenArray) {
            shuriken.update(delta);
            if (!shuriken.isAlive) {
                Gdx.app.log("FREE", "FREE");
                shurikenArray.removeValue(shuriken, true);
                shurikenPool.free(shuriken);
            }
        }
        //Update Player Position
        position.mulAdd(velocity, delta);
        headPosition.mulAdd(headVelocity, delta);
        //Update HitBox position
        hitBox.setPosition(position.x, position.y);
        //Update origin
        originX = hitBox.x + WIDTH / 2;
        originY = hitBox.y + HEIGHT / 2;
        headOriginX = headPosition.x + WIDTH / 2;
        headOriginY = headPosition.y + HEIGHT / 2;

    }
    //Actions

    public void jump() {
        if (alive)
            sfxJump.play(0.25f);
        velocity.y = JUMP_HEIGHT;
    }

    public void shootShuriken(int shootDirection) {
        if (alive) {
            currentPlayerState = PlayerState.ATTACK;
            playerDirection = shootDirection;
            shoot = true;
            if (shurikenShootTimer > 0.5f) {
                sfxShoot.play(0.25f);
                Shuriken shuriken = shurikenPool.obtain();
                shuriken.setPosition(position.x + 8, position.y + 5);
                shuriken.setDirection(shootDirection);

                shurikenArray.add(shuriken);

                shurikenShootTimer = 0;

                shotsFired ++;
            }
        }

    }

    public void draw(SpriteBatch batch) {
        if (blinker.shouldBlink(Gdx.graphics.getDeltaTime())) {
            return;
        }
        TextureRegion region = null;
        switch (currentPlayerState) {
            case IDLE:
                region = idleAnim.getKeyFrame(stateTimer);
                if (shoot) {
                    if (playerDirection > 0) {
                        region = attackRightAnim.getKeyFrame(stateTimer);
                    } else if (playerDirection < 0) {
                        region = attackLeftAnim.getKeyFrame(stateTimer);
                    }


                }
                break;
            case JUMP:
                region = jumpFrame;
                if (shoot) {
                    if (playerDirection > 0) {
                        region = attackRightAnim.getKeyFrame(stateTimer);
                    } else if (playerDirection < 0) {
                        region = attackLeftAnim.getKeyFrame(stateTimer);
                    }
                }
                break;
            case ATTACK:
                if (playerDirection > 0) {
                    region = attackRightAnim.getKeyFrame(stateTimer);
                } else if (playerDirection < 0) {
                    region = attackLeftAnim.getKeyFrame(stateTimer);
                }
                break;
        }
        for (Shuriken shuriken : shurikenArray) {
            shuriken.draw(game.batch);
        }
        if (alive) {
            batch.draw(region, position.x, position.y);
        } else {
            if (hitByShuriken) {
                batch.draw(deathFrame1, position.x, position.y);
                playerHead.draw(batch);
            } else {
                batch.draw(region, originX, originY, region.getRegionWidth() / 2, region.getRegionHeight() / 2, region.getRegionWidth(), region.getRegionHeight(), 1, 1, rotation, rotationClockWise);
            }

        }


    }

    public void drawDebug(ShapeRenderer shapeRenderer) {
        shapeRenderer.rect(hitBox.x, hitBox.y, hitBox.getWidth(), hitBox.getHeight());
    }
}
