package tk.indieme.changtheninja;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import de.golfgl.gdxgamesvcs.IGameServiceClient;
import de.golfgl.gdxgamesvcs.IGameServiceListener;
import de.golfgl.gdxgamesvcs.NoGameServiceClient;
import tk.indieme.changtheninja.helpers.AssetLoader;
import tk.indieme.changtheninja.screens.PlayScreen;
import tk.indieme.changtheninja.screens.SplashScreen;
import tk.indieme.changtheninja.screens.StartScreen;

public class ChangTheNinjaGame extends Game implements IGameServiceListener {
    public SpriteBatch batch;
    public AssetLoader assetLoader;
    public boolean debug = false;
    public StringFormatter stringFormatter;
    //Game Preference Settings
    public Preferences gamePref;
    //GPGS client
    public IGameServiceClient gameServiceClient;

    public ChangTheNinjaGame(StringFormatter stringFormatter) {
        this.stringFormatter = stringFormatter;
    }

    @Override
    public void create() {

        assetLoader = new AssetLoader();
        assetLoader.init();
        Texture.setAssetManager(assetLoader.getAssetManager());
        batch = new SpriteBatch();
        gamePref = Gdx.app.getPreferences("gamePref");
        //Initializing game service client
        if (gameServiceClient == null) {
			gameServiceClient = new NoGameServiceClient();
        }
        // for getting callbacks from the client
        gameServiceClient.setListener(this);

        // establish a connection to the game service without error messages or login screens
      //  gameServiceClient.resumeSession();
        gameServiceClient.logIn();
        setScreen(new PlayScreen(this));
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        super.render();
    }

    @Override
    public void pause() {
        super.pause();
        gameServiceClient.pauseSession();

    }

    @Override
    public void resume() {
        super.resume();

        gameServiceClient.resumeSession();

        //TODO: ASSETLOADER
    }

    @Override
    public void dispose() {
        batch.dispose();
        assetLoader.dispose();
        super.dispose();

    }

    @Override
    public void gsOnSessionActive() {

    }

    @Override
    public void gsOnSessionInactive() {

    }

    @Override
    public void gsShowErrorToUser(GsErrorType et, String msg, Throwable t) {

    }
}
